
/*  $Header$
 *
 *  Copyright (c) 1990-2004 Sun Microsystems, Inc.
 *  All Rights Reserved.
 */

Overview of changes in gperfmeter 2.0.48

* Added:

    intltool-extract.in
    intltool-merge.in
    intltool-update.in

  to the list of files that are included in a gperfmeter compressed tarball
  distribution.

* Added the "const" qualifier to three Gtk...ActionEntry definitions in 
  perfmeter.c

----

Overview of changes in gperfmeter 2.0.47

* Generated a tarball release.

----

Overview of changes in gperfmeter 2.0.46

* The "Local host:" UI component on the preferences sheet was not hooked up 
  to the PropsToggleCB callback in props.c.

----

Overview of changes in gperfmeter 2.0.45

* Fixed bug #163374. Right clicking over the labels in the meter drawing 
  areas, now correctly display the gperfmeter menu.

----

Overview of changes in gperfmeter 2.0.44

* Fixed bug #163282. gperfmeter now uses color .png images to show a 
  dead or sick host, rather the the previous monochrome .xpm files.

----

Overview of changes in gperfmeter 2.0.43

* Fixed bug #163281. Several problems:
  - One of the Glade attributes for the Hosts_Combo wasn't being set.
  - The LogFile_Entry widget wasn't connected up.
  - All the accessibility information in the Glade file had been lost.
  - Needed to check for a null widget being passed into the SetSensitive
    function in props.c.

----

Overview of changes in gperfmeter 2.0.42

* More work on bug #163290. Changes to fixup the Gtk-CRITICAL assertions
  that were occuring.

----

Overview of changes in gperfmeter 2.0.41

* More work on bug #163290. Replaces the GtkOptionMenu with a GtkComboBox.

----

Overview of changes in gperfmeter 2.0.40

* More work on bug #164324. A much cleaner solution. Also shares the 
  various similar menu items in the menu bar menus and the popup menu 
  over the chart area.

* More work on bug #163290. The GnomeColorPicker code has been changed to
  use GtkColorButton (including the needed changes in the gperfmeter.glade
  file). 

----

Overview of changes in gperfmeter 2.0.39

* Fixed bug #164324. Replaced all occurances of GtkItemFactory with GtkAction.

----

Overview of changes in gperfmeter 2.0.38

* Refixed up the three occurances where get_menubar_widget() should have
  been looking for "/View/Menubar" not "/View/Menu Bar", by adjusting
  the actual "/View/Men_ubar" definition in the GtkItemFactoryEntry struct.

* Refixed bug #162650. The GOption changes from the fix for bug #163310 
  caused a regression with the option displaying of the host name in the 
  graph display area when gperfmeter was restarted a subsequent time.

----

Overview of changes in gperfmeter 2.0.37

* Fixed up three occurances where get_menubar_widget() should have 
  been looking for "/View/Menu Bar" not "/View/Menubar".

----

Overview of changes in gperfmeter 2.0.36

* Partial fix for bug #163310. Gperfmeter now uses GOption calls rather 
  than popt for parsing command line options.

----

Overview of changes in gperfmeter 2.0.35

* Fixed bug #164199. Fixed a potential SEGV in the expand_tilde() routine.

* Small adjustment to the color_to_string() routine to simplify it.

----

Overview of changes in gperfmeter 2.0.34

* Partial fix for bug #163897. Removed usage of sprintf, printf, and 
  strcat and replaced them with the glib equivalents.

----

Overview of changes in gperfmeter 2.0.33

* Fixed bug #163280. New icon for gperfmeter from Eugenia Loli-Queru.

----

Overview of changes in gperfmeter 2.0.32

* Rest of the fix for enhancement #163289. More changes to get gperfmeter 
  to use glib routines for memory and string management.

----

Overview of changes in gperfmeter 2.0.31

* Partial fix for enhancement #163289. Various changes to get gperfmeter to 
  use glib routines for memory and string management.

----

Overview of changes in gperfmeter 2.0.30

* Fixed bug #163091. Slight adjustments to correct various menu item labels.

----

Overview of changes in gperfmeter 2.0.29

* Fixed bug #162650. gperfmeter now optionally displays the remote host name 
  (or "localhost") below the graph display area. This is set via a checkbox 
  on the Preferences sheet.

----

Overview of changes in gperfmeter 2.0.28

* Adjusted the AC_PROG_INTLTOOL definition in configure.in to not rely
  on a specific version.

----

Overview of changes in gperfmeter 2.0.27

* From Laszlo Peter <Laszlo dot Peter at Sun dot COM>
  Make gperfmeter install the .mo files in the correct directory on
  Solaris. The explanation is that "host" is defined by a macro called
  AC_CANONICAL_HOST and several other macros depend on its value,
  including AM_GLIB_GNU_GETTEXT.  Since it couldn't identify the system
  as Solaris, it put the locale stuff to /usr/lib/locale instead of
  /usr/share/locale.

* Adjusted all copyright messages to include 2004.

* Fleshed out the README file.

----

Overview of changes in gperfmeter 2.0.26

* gperfmeter.desktop.in was missing from the tarball distribution.

----

Overview of changes in gperfmeter 2.0.25

* xmldocs.make in top-level directory had three lines that didn't start 
  with a Tab.

* .../src/message.h, .../src/callbacks.h and .../src/perfmeter.xbm
  were missing from the distribution.

* Generated official tarball for Sun JDS release.

----

Overview of changes in gperfmeter 2.0.24

* Fixed bug #94767. There were problems with the way that gperfmeter tried 
  to inform the user when a remotely monitored host could no longer be 
  displayed.

----

Overview of changes in gperfmeter 2.0.23

* Fixed bug #124561. GtkItemFactory has been deprecated. Removed the 
  GTK_DISABLE_DEPRECATED flag.

----

Overview of changes in gperfmeter 2.0.22

* Fixed bug #122784. The -v, --version and --help output messages are 
  now properly localized.

----

Overview of changes in gperfmeter 2.0.21

* Fixed bug #111034. The gperfmeter property sheet now uses a GtkCombo widget 
  to contain the list of remote hostnames. Also the remote hostnames are 
  stored as gconf resources with keys of "HostName0" to "HostName9" rather 
  than a comma separated list keyed on "HostName".

----

Overview of changes in gperfmeter 2.0.20

* Fixed bug #118319. If you make changes to a running gperfmeter, then this 
  should reconfigure any other instances of gperfmeter that are running.
  Only the current one should be affected, and the gconf preferences should 
  be saved away, so that any new instances of gperfmeter that are started 
  will automatically pick them up.

----

Overview of changes in gperfmeter 2.0.19

* Added a test in DrawGraph to prevent Gdk-CRITICAL assertions occuring
  because a GC was NULL when a new meter was displayed for the first time.
* Code cleanup. Removed some more unwanted code after the recent UI rewrite.

----

Overview of changes in gperfmeter 2.0.18

* Fixed bug #117125. Gperfmeter now has a sane minimum size. This is 
  dependant upon whether the menubar is being shown, and the width of 
  the largest chart label.

----

Overview of changes in gperfmeter 2.0.17

* Removed unwanted variables and fixed up numerous warning messages reported 
  by lint.
* All the meters are now created the same size.

----

Overview of changes in gperfmeter 2.0.16

* Fixed up a SEGV in PmSaveResources when DEBUG was turned on.
* Remove a spurious couple of lines of debug left in gnome-perfmeter.c

----

Overview of changes in gperfmeter 2.0.15

* Major UI gperfmeter rewrite:
  - Removed the single canvas DrawingArea.
  - Created a gnome-perfmeter custom Gtk widget that consists of two Labels
    (meter_type and maxval), the DrawingArea for the chart and horizontal 
    and vertical separators that are shown if this isn't the first meter.
  - Removed and/or simplified a lot of the code related to the drawing of 
    these meters.

----

Overview of changes in gperfmeter 2.0.14

* Fixed bug #110563. gperfmeter no longer crashes for bar chart format in
  Xinerama mode.

----

Overview of changes in gperfmeter 2.0.13

* More work on bug #112667. Fixups to provide working alternate for
  platforms that don't have system calls such as setnetconfig.

----

Overview of changes in gperfmeter 2.0.12

* Fixed bug #112667. Changed from transport specific to transport 
  independent and now it accepts IPv6 addresses also.

----

Overview of changes in gperfmeter 2.0.11

* Forced gperfmeter build to be Gtk2 compliant

----

Overview of changes in gperfmeter 2.0.10

* Removed the gperfmeter.glade in the top-level directory to avoid any
  further confusion. 
* Removed the HostsEntryCB and LogFileEntryCB signal callbacks in
  .../src/gperfmeter.glade as they did nothing.

----

Overview of changes in gperfmeter 2.0.9

* Fixed bug #110793 (http://bugzilla.gnome.org/show_bug.cgi?id=110793):
  Gperfmeter now remembers it's last position and size and uses those      
  values when restarting.

----

Overview of changes in gperfmeter 2.0.8

* More code cleanup:
  - Removed the #ifdef FIX"'s from .../src/data.c. They aren't been
    used and there is no history to say why they are here (note they
    were also in the original sdtperfmeter code too in the same way).
  - Removed empty functions (and there setting if they are callbacks),
    such as: LogFileEntryCB and HostsEntryCB.
  - Removed all calls to PmStringCreateLocalized, making sure the string
    is surronded by _().
  - Fixed up numerous lint warnings.
  - Removed X11 specific calls where possible.

----

Overview of changes in gperfmeter 2.0.7

* Cleaned up .../src/resources.c and src/access/*.[c,h]:
  - Removed all tabs and some of the code that is commented out and/or not 
    needed.
  - Provided consistent indentation.

----

Overview of changes in gperfmeter 2.0.6

* Cleaned up more of the code:
  - Removed all tabs and some of the code that is commented out and/or not 
    needed.
  - Provided consistent indentation.

----

Overview of changes in gperfmeter 2.0.5

* Fixed Bugzilla bug #94770. The host name being monitored is now displayed 
  in the titlebar (or "localhost" when monitoring the local host).

----

Overview of changes in gperfmeter 2.0.4

* Fixed Bugzilla bug #110863. Adjusted various long and short descriptions 
  in gperfmeter.schemas.in to make their capitalisation consistent. 
  Plus "autoLoadByHost" becomes "AutoLoadByHost". 

----

Overview of changes in gperfmeter 2.0.3

* Fixed Bugzilla bug #93956. No longer need to press Return for new 
  threshold values to take place in the preferences window.

----

Overview of changes in gperfmeter 2.0.2

* Fixed Bugzilla bug #100015. Added double quotes (and semi-colon) to make 
  two schemas long messages easier to understand.

----

Overview of changes in gperfmeter 2.0.1

* Updated all copyright messages to be until 2003.
* Adjusted original ChangeLog entries to the correct format.
* Reversed order of entries in the AUTHORS file.
* Added entries to the NEWS file.
* Adjusted some of .../gperfmeter/src/perfmeter.c to the OneTrueStyle(TM).
* Removed "#ifdef FIXUP" entries and unwanted commented out code.
* Removed "#ifdef APPLET" entries and unwanted commented out code.
* Removed calls to PmResetDecor().

----

Overview of changes in gperfmeter 2.0.0

* Initial GNOME/Gtk+ version.

----
