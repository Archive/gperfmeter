<?xml version="1.0"?><!DOCTYPE article PUBLIC "-//OASIS//DTD DocBook XML V4.1.2//EN" 
"http://www.oasis-open.org/docbook/xml/4.1.2/docbookx.dtd" [
  <!ENTITY legal SYSTEM "legal.xml">
  <!ENTITY appversion "2.0.0">
  <!ENTITY manrevision "2.3">
  <!ENTITY date "October 2002">
  <!ENTITY app "Performance Meter">

]>

<!-- 
      (Do not remove this comment block.)
  Maintained by the GNOME Documentation Project
  http://developer.gnome.org/projects/gdp
  Template version: 2.0 beta
  Template last modified Feb 12, 2002
  
-->
<!-- =============Document Header ============================= -->
<article id="index" lang="fr">
<!-- please do not change the id; for translations, change lang to -->
<!-- appropriate code -->
  <articleinfo>
    <title>Manuel &app;  V&manrevision;</title>

    <copyright> 
      <year>2003</year> 
      <holder>Sun Microsystems</holder> 
    </copyright> 

<!-- translators: uncomment this:

  <copyright>
   <year>2002</year>
   <holder>ME-THE-TRANSLATOR (Latin translation)</holder>
  </copyright>

   -->

    <publisher> 
      <publishername>Projet de documentation GNOME</publishername> 
    </publisher> 

   &legal;

    <authorgroup>
      <author> 
	<firstname>Sun</firstname> 
	<surname>Equipe de documentation GNOME</surname> 
	<affiliation> 
	  <orgname>Sun Microsystems</orgname> 
	  <address> <email>gdocteam@sun.com</email> </address>
	</affiliation>
      </author>

<!-- This is appropriate place for other contributors: translators,
      maintainers,  etc. Commented out by default.
       <othercredit role="translator">
	<firstname>Latin</firstname> 
	<surname>Translator 1</surname> 
	<affiliation> 
	  <orgname>Latin Translation Team</orgname> 
	  <address> <email>translator@gnome.org</email> </address> 
	</affiliation>
	<contrib>Latin translation</contrib>
      </othercredit>
-->
    </authorgroup>
 
    <revhistory>
      <revision> 
	<revnumber>Manuel &app; V&manrevision;</revnumber> 
	<date>&date;</date> 
	<revdescription> 
	  <para role="author">Sun GNOME Documentation Team
	    <email>gdocteam@sun.com</email>
	  </para>
	  <para role="publisher">Projet de documentation GNOME</para>
	</revdescription>
      </revision>
      <revision> 
	<revnumber>Manuel du moniteur de performances V2.2</revnumber> 
	<date>Ao&ucirc;t 2002</date> 
	<revdescription> 
	  <para role="author">&Eacute;quipe de documentation GNOME de Sun
	    		<email>gdocteam@sun.com</email>
	  		 </para>
	  <para role="publisher">Projet de documentation GNOME</para>
	</revdescription>
      </revision>
      <revision> 
	<revnumber>Manuel du moniteur de performances V2.1</revnumber> 
	<date>Juin 2002</date> 
	<revdescription> 
	  <para role="author">&Eacute;quipe de documentation GNOME de Sun
	    		<email>gdocteam@sun.com</email>
	  		 </para>
	  <para role="publisher">Projet de documentation GNOME</para>
	</revdescription>
      </revision>
      <revision> 
	<revnumber>Manuel de moniteur de performances V2.0</revnumber> 
	<date>Mai 2002</date> 
	<revdescription> 
	  <para role="author">&Eacute;quipe de documentation GNOME de Sun
	    		<email>gdocteam@sun.com</email>
	  		 </para>
	  <para role="publisher">Projet de documentation GNOME</para>
	</revdescription>
      </revision>
   </revhistory>
   
    <releaseinfo>Le pr&eacute;sent manuel d&eacute;crit la version &appversion; de &app;.
    </releaseinfo>
    <legalnotice> 
      <title>Feedback</title>
      <para>Pour signaler un probl&egrave;me ou faire une suggestion concernant l'application &app; ou le pr&eacute;sent manuel, proc&eacute;dez comme indiqu&eacute; &agrave; la <ulink url="ghelp:gnome-feedback" type="help">GNOME Feedback Page</ulink>. 
      </para>
<!-- Translators may also add here feedback address for translations -->
    </legalnotice>
  </articleinfo>

  <indexterm> 
    <primary>Moniteur de performances</primary> 
  </indexterm> 

<!-- ============= Document Body ============================= -->
<!-- ============= Introduction ============================== -->
<!-- Use the Introduction section to give a brief overview of what
     the application is and what it does. -->
  <sect1 id="gperfmeter-introduction"> 
    <title>Introduction</title>
    <para><application>&app;</application> contr&ocirc;le les performances d'un syst&egrave;me et affiche un aper&ccedil;u graphique dynamique de celles-ci. L'application repr&eacute;sente les performances du syst&egrave;me pour les principaux param&egrave;tres sous la forme de graphiques en segments et d'histogrammes.</para>
  </sect1>
  
  <sect1 id="gperfmeter-getting-started"> 
     <title>D&eacute;marrage</title>
    
    <sect2 id="gperfmeter-to-start">
<title>D&eacute;marrage de &app;</title>
    <para>Vous pouvez d&eacute;marrer <application>&app;</application> en recourant &agrave; l'une des m&eacute;thodes suivantes :</para>
    <variablelist>
    <varlistentry>
    <term>Menu <guimenu>Applications</guimenu></term>
    <listitem>
    <para>S&eacute;lectionnez 
    <menuchoice>
    <guisubmenu>Outils syst&egrave;me </guisubmenu>
    <guimenuitem> &app;</guimenuitem>
    </menuchoice>. </para>
    </listitem>
    </varlistentry>
    <varlistentry>
    <term>Ligne de commande</term>
    <listitem>
    <para>Entrez <command>gnome-perfmeter</command>, puis appuyez sur <keycap>Entr&eacute;e</keycap>.</para>
    </listitem>
    </varlistentry>
    </variablelist>
    </sect2>
    
    <sect2 id="gperfmeter-when-you-start">
    <title>Lors du lancement d'&app;</title>
    <para>Lorsque vous lancez <application>&app;</application>, la fen&ecirc;tre suivante appara&icirc;t.</para>

	<!-- ==== Figure ==== -->
	<figure id="gperfmeter-window">
	<title>Fen&ecirc;tre &app; </title>
	<screenshot>
 	<mediaobject>
   	<imageobject>
     	<imagedata fileref="figures/gperfmeter_window.png" format="PNG"/>
   	</imageobject>
 	<textobject>
   	<phrase>Affiche la fen&ecirc;tre du moniteur de performances. La fen&ecirc;tre contient une barre d'outils Contenant les menus Afficher, Options et Aide. 
   	</phrase>
 	</textobject>
 	</mediaobject>
	</screenshot>
	</figure>
	<!-- ==== End of Figure ==== -->
		
    <para>La zone d'affichage <application>&app;</application> pr&eacute;sente les informations suivantes pour chacun des param&egrave;tres de performance contr&ocirc;l&eacute;s par <application>&app;</application> :</para>
	<itemizedlist>
	   <listitem>
	      <para>un graphique en segments qui affiche un historique r&eacute;cent de la performance ; La couleur du graphique change quand le param&egrave;tre de performance d&eacute;passe un seuil pr&eacute;d&eacute;fini.</para>
	   </listitem>
	   <listitem>
	      <para>un histogramme qui affiche la performance courant ; La couleur de l'histogramme change quand le param&egrave;tre de performance d&eacute;passe un seuil pr&eacute;d&eacute;fini.</para>
	   </listitem>
	   <listitem>
	      <para>le nom du param&egrave;tre de performance ;</para>
	   </listitem>
	   <listitem>
	      <para>la valeur maximum possible de la performance.</para>
	   </listitem>
	</itemizedlist>

	</sect2>	 
  </sect1>

<!-- ================ Usage ================================ -->
  <sect1 id="gperfmeter-usage"> 
    <title>Utilisation</title>
    
    <sect2 id="gperfmeter-perf-areas"> 
      <title>Affichage ou masquage des param&egrave;tres de performance</title>
      <para>Lorsque vous lancez <application>&app;</application> pour la premi&egrave;re fois, l'application n'affiche que les graphiques de performance relatifs &agrave; l'utilisation du processeur. Pour consulter les graphiques des autres param&egrave;tres de performance, s&eacute;lectionnez le menu ad hoc sous <menuchoice><guimenu>Afficher</guimenu> <guisubmenu>M&eacute;trique</guisubmenu></menuchoice>, de la mani&egrave;re suivante :</para>
      <informaltable>
         <tgroup cols="2">
            <colspec colname="COLSPEC0" colwidth="1.00*"/>
            <colspec colname="col1"/>
               <thead>
                  <row valign="top">
                     <entry colname="COLSPEC0">
                     <para>&Eacute;l&eacute;ment de menu <guisubmenu>M&eacute;trique</guisubmenu></para>
                     </entry>
                     <entry colname="col1">
                     <para><application>&app;</application> affiche...</para>
                     </entry>
		  </row>
               </thead>
               <tbody>
                  <row valign="top">
                      <entry colname="COLSPEC0">
                      <para>CPU</para>
                      </entry>
                      <entry colname="col1">
                      <para>Pourcentage de l'utilisation du processeur.</para>
                      </entry>
                  </row>
                  <row valign="top">
                      <entry colname="COLSPEC0">
                      <para>Charge</para>
                      </entry>
                      <entry colname="col1">
                      <para>Nombre moyen de processus ex&eacute;cutables par minute.</para>
                      </entry>
                  </row>
                  <row valign="top">
                      <entry colname="COLSPEC0">
                      <para>Disk</para>
                      </entry>
                      <entry colname="col1">
                      <para>Nombre de transferts de disques par seconde.</para>
                      </entry>
                  </row>
                  <row valign="top">
                      <entry colname="COLSPEC0">
                      <para>Page</para>
                      </entry>
                      <entry colname="col1">
                      <para>Nombre d'&eacute;changes de pages par seconde.</para>
                      </entry>
                  </row>
                  <row valign="top">
                      <entry colname="COLSPEC0">
                      <para>Cntx</para>
                      </entry>
                      <entry colname="col1">
                      <para>Nombre de changements de contexte par seconde.</para>
                      </entry>
                  </row>
                  <row valign="top">
                      <entry colname="COLSPEC0">
                      <para>Ech.</para>
                      </entry>
                      <entry colname="col1">
                      <para>Nombre d'&eacute;changes de t&acirc;ches par seconde.</para>
                      </entry>
                  </row>
                  <row valign="top">
                      <entry colname="COLSPEC0">
                      <para>Intr</para>
                      </entry>
                      <entry colname="col1">
                      <para>Nombre d'interruptions de p&eacute;riph&eacute;riques par seconde.</para>
                      </entry>
                  </row>
                  <row valign="top">
                      <entry colname="COLSPEC0">
                      <para>Paqts</para>
                      </entry>
                      <entry colname="col1">
                      <para>Nombre de paquets ethernet par seconde.</para>
                      </entry>
                  </row>
                  <row valign="top">
                      <entry colname="COLSPEC0">
                      <para>Coll</para>
                      </entry>
                      <entry colname="col1">
                      <para>Nombre de collisions ethernet par seconde.</para>
                      </entry>
                  </row>
                  <row valign="top">
                      <entry colname="COLSPEC0">
                      <para>Errs</para>
                      </entry>
                      <entry colname="col1">
                      <para>Nombre d'erreurs par seconde lorsque le syst&egrave;me r&eacute;cup&egrave;re les paquets.</para>
                      </entry>
                  </row>
               </tbody>
         </tgroup>
      </informaltable>
      <para>Lorsque vous s&eacute;lectionnez un &eacute;l&eacute;ment du menu <menuchoice><guimenu>Afficher</guimenu> <guisubmenu>M&eacute;trique</guisubmenu></menuchoice>, <application>&app;</application>
      ajoute les graphiques relatifs au param&egrave;tre de performance dans la zone d'affichage de la fen&ecirc;tre <application>&app;</application>. Pour masquer les graphiques d'un param&egrave;tre de performance, d&eacute;s&eacute;lectionnez l'&eacute;l&eacute;ment de menu appropri&eacute;. </para>
    </sect2>

    <sect2 id="gperfmeter-view-bar"> 
      <title>Choix du type de graphiques &agrave; afficher</title>
      <para>Pour s&eacute;lectionner le type de graphiques &agrave; afficher dans la fen&ecirc;tre d'application, proc&eacute;dez comme suit :</para>
      <itemizedlist>
      <listitem>
      <para>Pour afficher un histogramme, s&eacute;lectionnez
      <menuchoice>
      <guimenu>Afficher</guimenu>
      <guimenuitem>Histogramme</guimenuitem>
      </menuchoice>.</para>
      </listitem>
      <listitem>
      <para>Pour afficher un graphique en segments, s&eacute;lectionnez
      <menuchoice>
      <guimenu>Afficher</guimenu>
      <guimenuitem>Graphique en segments</guimenuitem>
      </menuchoice>.</para>
      </listitem>
      <listitem>
      <para>Pour afficher les deux types de graphiques, s&eacute;lectionnez
      <menuchoice>
      <guimenu>Afficher</guimenu>
      <guimenuitem>Les deux graphiques</guimenuitem>
      </menuchoice>.</para>
      </listitem>
      </itemizedlist>
    </sect2>
       
    
    <sect2 id="gperfmeter-view-system"> 
      <title>Contr&ocirc;le de la performance d'un syst&egrave;me distant</title>
      <para>Pour contr&ocirc;ler la performance d'un syst&egrave;me distant, proc&eacute;dez comme suit :</para>
      <orderedlist>
         <listitem>
            <para>S&eacute;lectionnez 
            <menuchoice>
            <guimenu>&Eacute;dition</guimenu> 
            <guimenuitem>Pr&eacute;f&eacute;rences</guimenuitem>
            </menuchoice> pour afficher la bo&icirc;te de dialogue <guilabel>Pr&eacute;f&eacute;rences du moniteur de performances</guilabel>.</para>
         </listitem>
         <listitem>
            <para>S&eacute;lectionnez l'option <guilabel>Distant</guilabel> dans le groupe <guilabel>Hôte</guilabel>.</para>
         </listitem>
         <listitem>
            <para>Entrez le nom du syst&egrave;me distant &agrave; contr&ocirc;ler dans la zone de texte <guilabel>Distant</guilabel>. </para>
         </listitem>
         <listitem>
            <para>Cliquez sur le bouton <guibutton>OK</guibutton> pour appliquer la modification et fermer la bo&icirc;te de dialogue.</para>
         </listitem>
      </orderedlist>
    </sect2>

  </sect1>
     
  <!-- ============= Preferences ============================= -->
  <sect1 id="gperfmeter-prefs"> 
     <title>Pr&eacute;f&eacute;rences</title>
    <para>Pour configurer <application>&app;</application>, s&eacute;lectionnez 
      <menuchoice> 
	<guimenu>Édition</guimenu> 
	<guimenuitem>Pr&eacute;f&eacute;rences</guimenuitem> 
      </menuchoice>. La bo&icirc;te de dialogue <guilabel>Pr&eacute;f&eacute;rences du moniteur de performances</guilabel> contient les onglets suivants :</para>
    <itemizedlist>
      <listitem>
	<para> 
	  <xref linkend="gperfmeter-prefs-view"/></para>
      </listitem>
      <listitem>
	<para> 
	  <xref linkend="gperfmeter-prefs-preferences"/></para>
      </listitem>
    </itemizedlist>

    <sect2 id="gperfmeter-prefs-view"> 
      <title>M&eacute;trique</title>
      <variablelist>
	<varlistentry> 
	  <term> 
	    <guilabel>Hôte</guilabel> </term> 
	  <listitem>
	    <para>S&eacute;lectionnez l'une des options suivantes :</para>
	    <itemizedlist>
	      <listitem>
		<para> 
		  <guilabel>Local</guilabel> </para>
		<para>S&eacute;lectionnez cette option pour contr&ocirc;ler et afficher les performances du syst&egrave;me local.</para>
	      </listitem>
	      <listitem>
		<para> 
		  <guilabel>Distant</guilabel> </para>
		<para>S&eacute;lectionnez cette option pour contr&ocirc;ler et afficher les performances d'un syst&egrave;me distant. Si vous s&eacute;lectionnez cette option, entrez le nom du syst&egrave;me distant souhait&eacute; dans la zone de texte. </para>
	      </listitem>
	    </itemizedlist>
	    <!-- <para>Par d&eacute;faut : 	      <guilabel>Local</guilabel>.</para>  -->
	  </listitem>
	</varlistentry>
	<varlistentry> 
	  <term> 
	    Table m&eacute;trique </term> 
	  <listitem>
	    <para>La table de l'onglet <guilabel>Metricsx</guilabel> contient les param&egrave;tres suivants :</para>
	    <informaltable>
	    <tgroup cols="2">
            <colspec colname="COLSPEC0" colwidth="1.00*"/>
            <colspec colname="col1"/>
               <thead>
                  <row valign="top">
                     <entry>
                     <para>Nom de la colonne</para>
                     </entry>
                     <entry>
                     <para>Description</para>
                     </entry>
		  </row>
               </thead>
               <tbody>
                  <row valign="top">
                      <entry>
                      <para>Affichage</para>
                      </entry>
                      <entry>
                      <para>Cette colonne affiche les noms des param&egrave;tres de performance que l'application peut contr&ocirc;ler. S&eacute;lectionnez la case &agrave; cocher situ&eacute;e &agrave; c&ocirc;t&eacute; du param&egrave;tre de performance pour consulter les graphiques de performance dans la zone d'affichage de l'application.</para>
                      <!-- <para>Par d&eacute;faut : <guilabel>Proc</guilabel> toutes les autres cases sont d&eacute;coch&eacute;es</para> -->
                      </entry>
                  </row>
                  <row valign="top">
                      <entry>
                      <para>Seuil</para>
                      </entry>
                      <entry>
                      <para>Cette colonne affiche un seuil de performance pour chaque param&egrave;tre. Pour modifier le seuil d'un param&egrave;tre, entrez en un nouveau dans la zone de texte.
                      </para>
                      <!-- <para>Valeurs par d&eacute;faut : Les valeurs suivantes repr&eacute;sentent les seuils par d&eacute;faut :</para> -->
                      <itemizedlist>
                      <listitem>
                      <para>Processeur = 50</para>
                      </listitem>
                      <listitem>
                      <para>Charge = 2</para>
                      </listitem>
                      <listitem>
                      <para>Disque = 20</para>
                      </listitem>
                      <listitem>
                      <para>Page = 8</para>
                      </listitem>
                      <listitem>
                      <para>Contexte = 32</para>
                      </listitem>
                      <listitem>
                      <para>Echange = 2</para>
                      </listitem>
                      <listitem>
                      <para>Interruption = 50</para>
                      </listitem>
                      <listitem>
                      <para>Paquets = 16</para>
                      </listitem>
                      <listitem>
                      <para>Collisions = 2</para>
                      </listitem>
                      <listitem>
                      <para>Erreurs =2</para>
                      </listitem>
                      </itemizedlist>
                      </entry>
                  </row>
                  <row valign="top">
                      <entry>
                      <para>Inf&eacute;rieur</para>
                      </entry>
                      <entry>
                      <para>Cette colonne affiche la couleur que <application>&app;</application> utilise pour indiquer la performance du syst&egrave;me pour chaque param&egrave;tre lorsque celle-ci est inf&eacute;rieure au seuil sp&eacute;cifi&eacute; dans la colonne <guilabel>Seuil</guilabel>.
Pour s&eacute;lectionner une autre couleur, cliquez sur la couleur pour afficher la palette.</para>
                      <!-- <para>Valeur par d&eacute;faut : Bleu.</para> -->
                      </entry>
                  </row>
                  <row valign="top">
                      <entry>
                      <para>Sup&eacute;rieur</para>
                      </entry>
                      <entry>
                      <para>Cette colonne affiche la couleur que <application>&app;</application> utilise pour indiquer la performance du syst&egrave;me pour chaque param&egrave;tre lorsque celle-ci est sup&eacute;rieure au seuil sp&eacute;cifi&eacute; dans la colonne <guilabel>Seuil</guilabel>.
Pour s&eacute;lectionner une autre couleur, cliquez sur la couleur pour afficher la palette.</para>
                      <!-- <para>Valeur par d&eacute;faut : Rouge.</para> -->
                      </entry>
                  </row>
               </tbody>
          </tgroup>
          </informaltable>
	  </listitem>
	</varlistentry>
      </variablelist>
    </sect2>

    <sect2 id="gperfmeter-prefs-preferences"> 
    <title>Graphique</title>
	<variablelist>
	  <varlistentry> 
	    <term> 
	      <guilabel>Graphe</guilabel> </term> 
	    <listitem>
	      <para>Pour indiquer le type de graphique en segments affich&eacute; par <application>&app;</application>, s&eacute;lectionnez l'une des options suivantes :</para>
	      <itemizedlist>
		<listitem>
		  <para> 
		    <guilabel>Ligne</guilabel> </para>
		  <para>S&eacute;lectionnez cette option pour afficher un graphique en segments lin&eacute;aire.</para>
		</listitem>
		<listitem>
		  <para> 
		    <guilabel>Plein</guilabel> </para>
		  <para>S&eacute;lectionnez cette option pour afficher un graphique en segments plein.</para>
		</listitem>
	      </itemizedlist>
	      <!-- <para>Par d&eacute;faut: 
		<guilabel>Ligne</guilabel>.</para>  -->
	    </listitem>
	  </varlistentry>
	  <varlistentry> 
	    <term> 
	      <guilabel>Agencement</guilabel> </term> 
	    <listitem>
	      <para>Pour d&eacute;finir comment <application>&app;</application> affiche plusieurs graphiques de performance dans la fen&ecirc;tre <application>&app;</application>, s&eacute;lectionnez l'une des options suivantes :</para>
	      <itemizedlist>
		<listitem>
		  <para> 
		    <guilabel>Horizontal</guilabel> </para>
		  <para>S&eacute;lectionnez cette option pour afficher les graphiques de chaque param&egrave;tre c&ocirc;te &agrave; c&ocirc;te dans un sens horizontal.</para>
		</listitem>
		<listitem>
		  <para> 
		    <guilabel>Vertical</guilabel> </para>
		  <para>S&eacute;lectionnez cette option pour empiler les graphiques de chaque param&egrave;tre dans un sens vertical.</para>
		</listitem>
	      </itemizedlist>
	      <!-- <para>Par d&eacute;faut : 
		<guilabel>Vertical</guilabel>.</para>  -->
	    </listitem>
	  </varlistentry>
	  <varlistentry> 
	    <term> 
	      <guilabel>Affichage</guilabel> </term> 
	    <listitem>
	      <para>Pour indiquer le degr&eacute; de d&eacute;tails que <application>&app;</application> affiche pour chaque param&egrave;tre de performance, s&eacute;lectionnez l'une des options suivantes :</para>
	      <itemizedlist>
		<listitem>
		  <para> 
		    <guilabel>Histogramme</guilabel> </para>
		  <para>S&eacute;lectionnez cette option pour n'afficher qu'un histogramme indiquant la performance actuelle.</para>
		</listitem>
		<listitem>
		  <para> 
		    <guilabel>Graphique en segments</guilabel> </para>
		  <para>S&eacute;lectionnez cette option pour n'afficher qu'un graphique en segments indiquant un historique r&eacute;cent de la performance.</para>
		</listitem>
		<listitem>
		  <para> 
		    <guilabel>Les deux</guilabel> </para>
		  <para>S&eacute;lectionnez cette option pour afficher &agrave; la fois le graphique en segments et l'histogramme.</para>
		</listitem>
	      </itemizedlist>
	      <!-- <para>Par d&eacute;faut : 
		<guilabel>Les deux</guilabel>.</para>  -->
	    </listitem>
	  </varlistentry>
	  <varlistentry> 
	    <term> 
	      <guilabel>Ligne limite</guilabel> </term> 
	    <listitem>
	      <para>S&eacute;lectionnez cette option pour afficher une ligne repr&eacute;sentant le seuil de performance dans les graphiques. </para>
	      <!-- <para>Par d&eacute;faut: d&eacute;s&eacute;lectionn&eacute;e.</para>  -->
	    </listitem>
	  </varlistentry>
	  <varlistentry> 
	    <term> 
	      <guilabel>Masquer la barre de menus</guilabel> </term> 
	    <listitem>
	      <para>S&eacute;lectionnez cette option pour masquer la barre de menus. Cliquez dans la zone d'affichage pour la faire r&eacute;appara&icirc;tre.</para>
	      <!-- <para>Par d&eacute;faut: d&eacute;s&eacute;lectionn&eacute;e.</para>  -->
	    </listitem>
	  </varlistentry>
	  <varlistentry> 
	    <term> 
	      <guilabel>Journaliser les &eacute;chantillons dans</guilabel> </term> 
	    <listitem>
	      <para>S&eacute;lectionnez cette option pour enregistrer dans un journal toutes les valeurs de performance affich&eacute;es par <application>&app;</application> dans les graphiques en segments. La zone de texte permet d'entrer le nom du fichier dans lequel <application>&app;</application> enregistre les donn&eacute;es.</para>
	      <!-- <para>Par d&eacute;faut: d&eacute;s&eacute;lectionn&eacute;e.</para> -->
	    </listitem>
	  </varlistentry>
	  <varlistentry> 
	    <term> 
	      <guilabel>Échantillonner toutes les</guilabel> </term> 
	    <listitem>
	      <para>Cette zone d&eacute;roulante permet d'indiquer l'intervalle auquel <application>&app;</application> effectue un &eacute;chantillonnage de la performance.</para>
	      <!-- <para>Par d&eacute;faut : 1 seconde.</para>  -->
	    </listitem>
	  </varlistentry>  
	</variablelist>
    </sect2>
  </sect1>
</article>
