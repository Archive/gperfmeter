<?xml version="1.0"?><!DOCTYPE article PUBLIC "-//OASIS//DTD DocBook XML V4.1.2//EN" 
"http://www.oasis-open.org/docbook/xml/4.1.2/docbookx.dtd" [
  <!ENTITY legal SYSTEM "legal.xml">
  <!ENTITY appversion "2.0.0">
  <!ENTITY manrevision "2.3">
  <!ENTITY date "Ottobre 2002">
  <!ENTITY app "Misurazione prestazioni">

]>

<!-- 
      (Do not remove this comment block.)
  Maintained by the GNOME Documentation Project
  http://developer.gnome.org/projects/gdp
  Template version: 2.0 beta
  Template last modified Feb 12, 2002
  
-->
<!-- =============Document Header ============================= -->
<article id="index" lang="it">
<!-- please do not change the id; for translations, change lang to -->
<!-- appropriate code -->
  <articleinfo>
     <title>Manuale di &app; V&manrevision;</title>

    <copyright> 
      <year>2003</year> 
      <holder>Sun Microsystems</holder> 
    </copyright> 

<!-- translators: uncomment this:

  <copyright>
   <year>2002</year>
   <holder>ME-THE-TRANSLATOR (Latin translation)</holder>
  </copyright>

   -->

    <publisher> 
      <publishername>GNOME Documentation Project</publishername> 
    </publisher> 

   &legal;

    <authorgroup>
      <author> 
	<firstname>Sun</firstname> 
	<surname>GNOME Documentation Team</surname> 
	<affiliation> 
	  <orgname>Sun Microsystems</orgname> 
	  <address> <email>gdocteam@sun.com</email> </address>
	</affiliation>
      </author>

<!-- This is appropriate place for other contributors: translators,
      maintainers,  etc. Commented out by default.
       <othercredit role="translator">
	<firstname>Latin</firstname> 
	<surname>Translator 1</surname> 
	<affiliation> 
	  <orgname>Latin Translation Team</orgname> 
	  <address> <email>translator@gnome.org</email> </address> 
	</affiliation>
	<contrib>Latin translation</contrib>
      </othercredit>
-->
    </authorgroup>
 
    <revhistory>
      <revision> 
	<revnumber>Manuale di &app; V&manrevision;</revnumber> 
	<date>&date;</date> 
	<revdescription> 
	  <para role="author">Sun GNOME Documentation Team
	    <email>gdocteam@sun.com</email>
	  </para>
	  <para role="publisher">GNOME Documentation Project</para>
	</revdescription>
      </revision>
      <revision> 
	<revnumber>Manuale di Misurazione prestazioni V2.2</revnumber> 
	<date>Agosto 2002</date> 
	<revdescription> 
	  <para role="author">Sun GNOME Documentation Team
	    <email>gdocteam@sun.com</email>
	  </para>
	  <para role="publisher">GNOME Documentation Project</para>
	</revdescription>
      </revision>
      <revision> 
	<revnumber>Manuale di Misurazione prestazioni V2.1</revnumber> 
	<date>Giugno 2002</date> 
	<revdescription> 
	  <para role="author">Sun GNOME Documentation Team
	    <email>gdocteam@sun.com</email>
	  </para>
	  <para role="publisher">GNOME Documentation Project</para>
	</revdescription>
      </revision>
      <revision> 
	<revnumber>Manuale di Misurazione prestazioni V2.0</revnumber> 
	<date>Maggio 2002</date> 
	<revdescription> 
	  <para role="author">Sun GNOME Documentation Team
	    <email>gdocteam@sun.com</email>
	  </para>
	  <para role="publisher">GNOME Documentation Project</para>
	</revdescription>
      </revision>
   </revhistory>
   
    <releaseinfo>Questo manuale descrive la versione &appversion; di &app;.
    </releaseinfo>
    <legalnotice> 
        <title>Commenti</title>
      <para>Per segnalare un problema o inviare suggerimenti sull'applicazione &app; o su questo manuale, seguire le istruzioni presenti alla <ulink url="ghelp:gnome-feedback" type="help">Pagina di commenti su GNOME</ulink>. 
      </para>
<!-- Translators may also add here feedback address for translations -->
    </legalnotice>
  </articleinfo>

  <indexterm> 
    <primary>Misurazione prestazioni</primary> 
  </indexterm> 

<!-- ============= Document Body ============================= -->
<!-- ============= Introduction ============================== -->
<!-- Use the Introduction section to give a brief overview of what
     the application is and what it does. -->
  <sect1 id="gperfmeter-introduction">
      <title>Introduzione</title>
    <para>L'applicazione <application>&app;</application> monitorizza le prestazioni del sistema e visualizza dinamicamente i dati rilevati in forma di diagramma. I parametri principali delle prestazioni del sistema vengono visualizzati mediante diagrammi a linee e a barre.</para>
  </sect1>
  
  <sect1 id="gperfmeter-getting-started"> 
    <title>Per iniziare</title>
    
    <sect2 id="gperfmeter-to-start">
    <title>Avviare la &app;</title>
    <para>Sono disponibili i seguenti metodi per avviare la <application>&app;</application>:</para>
    <variablelist>
    <varlistentry>
    <term>Menu <guimenu>Programmi</guimenu></term>
    <listitem>
    <para>Scegliere <menuchoice>
    <guisubmenu>Sistema</guisubmenu>
    <guimenuitem> &app;</guimenuitem>
    </menuchoice>. </para>
    </listitem>
    </varlistentry>
    <varlistentry>
    		<term>Riga di comando</term>
    		 <listitem>
    <para>Digitare <command>gnome-perfmeter</command> e premere <keycap>Return</keycap>.</para>
    </listitem>
    </varlistentry>
    </variablelist>
    </sect2>
    
    <sect2 id="gperfmeter-when-you-start">  		<title>Avvio della &app;</title>
    <para>All'avvio della <application>&app;</application> viene visualizzata la finestra seguente.</para>

	<!-- ==== Figure ==== -->
	<figure id="gperfmeter-window">
	<title>Finestra della &app;</title>
	<screenshot>
 	<mediaobject>
   	<imageobject>
     	<imagedata fileref="figures/gperfmeter_window.png" format="PNG"/>
   	</imageobject>
 	<textobject>
   	<phrase>
   	Mostra la finestra di Misurazione prestazioni. La finestra contiene una barra degli strumenti con i menu Visualizza, Opzioni e Guida. 
   	</phrase>
 	</textobject>
 	</mediaobject>
	</screenshot>
	</figure>
	<!-- ==== End of Figure ==== -->
		
    <para>L'area di visualizzazione della <application>&app;</application> presenta le seguenti informazioni sui parametri monitorati:</para>
	<itemizedlist>
	   <listitem>
	      <para>Un istogramma con la cronologia delle prestazioni pi&ugrave; recenti. Il colore dell'istogramma cambia quando il parametro rilevato supera la soglia predefinita.</para>
	   </listitem>
	   <listitem>
	      <para>Un diagramma a barre con le prestazioni correnti. Il colore del diagramma cambia quando il parametro rilevato supera la soglia predefinita.</para>
	   </listitem>
	   <listitem>
	      <para>Il nome del parametro misurato.</para>
	   </listitem>
	   <listitem>
	      <para>Il valore massimo raggiungibile.</para>
	   </listitem>
	</itemizedlist>

	</sect2>	 
  </sect1>

<!-- ================ Usage ================================ -->
  <sect1 id="gperfmeter-usage">  
     <title>Utilizzo</title>
    
    <sect2 id="gperfmeter-perf-areas"> 
      <title>Visualizzare o nascondere i parametri delle prestazioni</title>
      <para>Al primo avvio della <application>&app;</application>, vengono visualizzati solo i diagrammi di prestazioni relativi all'utilizzo della CPU. Per visualizzare i diagrammi relativi ad altri parametri, selezionare l'opzione appropriata dal menu <menuchoice><guimenu>Visualizza</guimenu> <guisubmenu>Parametri</guisubmenu></menuchoice> come segue:</para>
      <informaltable>
         <tgroup cols="2">
            <colspec colname="COLSPEC0" colwidth="1.00*"/>
            <colspec colname="col1"/>
               <thead>
                  <row valign="top">
                     <entry colname="COLSPEC0">
                     <para>Opzione <guisubmenu>Parametri</guisubmenu> </para>
                     </entry>
                     <entry colname="col1">
                     <para>Valore visualizzato dalla <application>&app;</application></para>
                     </entry>
		  </row>
               </thead>
               <tbody>
                  <row valign="top">
                      <entry colname="COLSPEC0">
                      <para>CPU</para>
                      </entry>
                      <entry colname="col1">
                      <para>Percentuale di utilizzo della CPU.</para>
                      </entry>
                  </row>
                  <row valign="top">
                      <entry colname="COLSPEC0">
                      <para>Carico</para>
                      </entry>
                      <entry colname="col1">
                      <para>Numero medio di processi eseguibili al minuto.</para>
                      </entry>
                  </row>
                  <row valign="top">
                      <entry colname="COLSPEC0">
                      <para>Disco</para>
                      </entry>
                      <entry colname="col1">
                      <para>Numero di trasferimenti da/verso il disco al secondo.</para>
                      </entry>
                  </row>
                  <row valign="top">
                      <entry colname="COLSPEC0">
                      <para>Pagina</para>
                      </entry>
                      <entry colname="col1">
                      <para>Numero di cambiamenti di pagina al secondo.</para>
                      </entry>
                  </row>
                  <row valign="top">
                      <entry colname="COLSPEC0">
                      <para> Contesto</para>
                      </entry>
                      <entry colname="col1">
                      <para>Numero di cambiamenti di contesto al secondo.</para>
                      </entry>
                  </row>
                  <row valign="top">
                      <entry colname="COLSPEC0">
                      <para>Cambiamenti lavoro</para>
                      </entry>
                      <entry colname="col1">
                      <para>Numero di cambiamenti di lavoro al secondo.</para>
                      </entry>
                  </row>
                  <row valign="top">
                      <entry colname="COLSPEC0">
                      <para>Interruzioni</para>
                      </entry>
                      <entry colname="col1">
                      <para>Numero di interruzioni dei dispositivi al secondo.</para>
                      </entry>
                  </row>
                  <row valign="top">
                      <entry colname="COLSPEC0">
                      <para>Pacchetti</para>
                      </entry>
                      <entry colname="col1">
                      <para>Numero di pacchetti Ethernet al secondo.</para>
                      </entry>
                  </row>
                  <row valign="top">
                      <entry colname="COLSPEC0">
                      <para>Collisioni</para>
                      </entry>
                      <entry colname="col1">
                      <para>Numero di collisioni Ethernet al secondo.</para>
                      </entry>
                  </row>
                  <row valign="top">
                      <entry colname="COLSPEC0">
                      <para>Errori</para>
                      </entry>
                      <entry colname="col1">
                      <para>Numero di errori al secondo durante il richiamo di pacchetti.</para>
                      </entry>
                  </row>
               </tbody>
         </tgroup>
      </informaltable>
      <para>Quando si seleziona una voce dal menu <menuchoice><guimenu>Visualizza</guimenu> <guisubmenu>Parametri</guisubmenu></menuchoice>, la <application>&app;</application>
      aggiunge il diagramma delle prestazioni per quel parametro nell'area di visualizzazione della finestra della  <application>&app;</application>. Per nascondere il diagramma relativo a un parametro, deselezionare l'opzione corrispondente. </para>
    </sect2>

    <sect2 id="gperfmeter-view-bar"> 
      <title>Scegliere il tipo di diagramma da visualizzare</title>
      <para>Per scegliere il tipo di diagramma da visualizzare nella finestra dell'applicazione, procedere come segue:</para>
      <itemizedlist>
      <listitem>
      <para>Per visualizzare i diagrammi a barre, scegliere <menuchoice>
      <guimenu>Visualizza</guimenu>
      <guimenuitem>Diagramma a barre</guimenuitem>
      </menuchoice>.</para>
      </listitem>
      <listitem>
      <para>Per visualizzare gli istogrammi, scegliere 
<menuchoice>
      <guimenu>Visualizza</guimenu>
      <guimenuitem>Istogramma</guimenuitem>
      </menuchoice>.</para>
      </listitem>
      <listitem>
      <para>Per visualizzare sia i diagrammi a barre che gli istogrammi, scegliere 
      <menuchoice>
      <guimenu>Visualizza</guimenu>
      <guimenuitem>Entrambi</guimenuitem>
      </menuchoice>.</para>
      </listitem>
      </itemizedlist>
    </sect2>
       
    
    <sect2 id="gperfmeter-view-system"> 
      <title>Monitorare le prestazioni di un sistema remoto</title>
      <para>Per monitorare le prestazioni di un sistema remoto, procedere come segue:</para>
      <orderedlist>
         <listitem>
            <para>Scegliere <menuchoice>
            <guimenu>Modifica</guimenu> 
            <guimenuitem>Preferenze</guimenuitem>
            </menuchoice> per aprire la finestra di dialogo <guilabel>Preferenze per la Misurazione prestazioni</guilabel>.</para>
         </listitem>
         <listitem>
            <para>Selezionare l'opzione <guilabel>Remoto</guilabel> nel gruppo <guilabel>Host</guilabel>.</para>
         </listitem>
         <listitem>
            <para>Digitare il nome del sistema remoto che si desidera monitorare nella casella di testo <guilabel>Remoto</guilabel>. </para>
         </listitem>
         <listitem>
            <para>Fare clic su <guibutton>OK</guibutton> per applicare le modifiche e chiudere la finestra di dialogo.</para>
         </listitem>
      </orderedlist>
    </sect2>

  </sect1>
     
  <!-- ============= Preferences ============================= -->
  <sect1 id="gperfmeter-prefs"> 
    <title>Preferenze</title>
    <para>Per configurare la <application>&app;</application>, scegliere 
      <menuchoice> 
	<guimenu>Modifica</guimenu> 
	<guimenuitem>Preferenze</guimenuitem> 
      </menuchoice>. La finestra di dialogo <guilabel>Preferenze per la Misurazione prestazioni</guilabel> contiene le seguenti schede:</para>
    <itemizedlist>
      <listitem>
	<para> 
	  <xref linkend="gperfmeter-prefs-view"/></para>
      </listitem>
      <listitem>
	<para> 
	  <xref linkend="gperfmeter-prefs-preferences"/></para>
      </listitem>
    </itemizedlist>

    <sect2 id="gperfmeter-prefs-view"> 
      <title>Parametri</title>
      <variablelist>
	<varlistentry> 
	  <term> 
	    <guilabel>Host</guilabel> </term> 
	  <listitem>
	    <para>Selezionare una delle seguenti opzioni:</para>
	    <itemizedlist>
	      <listitem>
		<para> 
		  <guilabel>Locale</guilabel> </para>
		<para>Selezionare questa opzione per monitorare e visualizzare le prestazioni del sistema locale.</para>
	      </listitem>
	      <listitem>
		<para> 
		  <guilabel>Remoto</guilabel> </para>
		<para>Selezionare questa opzione per monitorare e visualizzare le prestazioni di un sistema remoto. Se viene selezionata questa opzione, occorre digitare nel campo di testo il nome del sistema remoto che si desidera monitorare. </para>
	      </listitem>
	    </itemizedlist>
	    <!-- <para>Default: 
	      <guilabel>Local</guilabel>.</para>  -->
	  </listitem>
	</varlistentry>
	<varlistentry> 
	  <term> 
	    Tabella dei parametri</term> 
	  <listitem>
	    <para>La tabella della scheda <guilabel>Parametri</guilabel> contiene le seguenti impostazioni:</para>
	    <informaltable>
	    <tgroup cols="2">
            <colspec colname="COLSPEC0" colwidth="1.00*"/>
            <colspec colname="col1"/>
               <thead>
                  <row valign="top">
                     <entry>
                     <para>Titolo della colonna</para>
                     </entry>
                     <entry>
                     <para>Descrizione</para>
                     </entry>
		  </row>
               </thead>
               <tbody>
                  <row valign="top">
                      <entry>
                      <para>Visualizza</para>
                      </entry>
                      <entry>
                      <para>Questa colonna presenta i nomi dei parametri che &egrave; possibile monitorare. Selezionare la casella posta accanto al parametro di interesse per visualizzare il relativo diagramma nell'area di visualizzazione dell'applicazione.</para>
                      <!-- <para>Defaults: <guilabel>CPU</guilabel> selected, all other checkboxes unselected</para> -->
                      </entry>
                  </row>
                  <row valign="top">
                      <entry>
                      <para>Soglia</para>
                      </entry>
                      <entry>
                      <para>Questa colonna visualizza il valore di soglia per ogni parametro. Per modificare il valore di soglia per un parametro, digitare il nuovo valore nella casella di testo.
                      </para>
                      <!-- <para>Defaults: The following are the default thresholds:</para> -->
                      <itemizedlist>
                      <listitem>
                      <para>CPU = 50</para>
                      </listitem>
                      <listitem>
                      <para>Carico = 2</para>
                      </listitem>
                      <listitem>
                      <para>Disco = 20</para>
                      </listitem>
                      <listitem>
                      <para>Pagina = 8</para>
                      </listitem>
                      <listitem>
                      <para>Contesto = 32</para>
                      </listitem>
                      <listitem>
                      <para>Swap = 2</para>
                      </listitem>
                      <listitem>
                      <para>Interruzioni = 50</para>
                      </listitem>
                      <listitem>
                      <para>Pacchetti = 16</para>
                      </listitem>
                      <listitem>
                      <para>Collisioni = 2</para>
                      </listitem>
                      <listitem>
                      <para>Errori =2</para>
                      </listitem>
                      </itemizedlist>
                      </entry>
                  </row>
                  <row valign="top">
                      <entry>
                      <para>Sotto</para>
                      </entry>
                      <entry>
                      <para>Questa colonna mostra il colore utilizzato dalla <application>&app;</application> per visualizzare le prestazioni del sistema per ogni parametro quando i valori rilevati sono al di sotto della soglia specificata nella colonna <guilabel>Soglia</guilabel>.
                      Per selezionare un colore differente, fare clic sul colore per visualizzare la tavolozza.</para>
                      <!-- <para>Defaults: blue.</para> -->
                      </entry>
                  </row>
                  <row valign="top">
                      <entry>
                      <para>Sopra</para>
                      </entry>
                      <entry>
                      <para>Questa colonna mostra il colore utilizzato dalla <application>&app;</application> per visualizzare le prestazioni del sistema per ogni parametro quando i valori rilevati sono al di sopra della soglia specificata nella colonna <guilabel>Soglia</guilabel>.
                      Per selezionare un colore differente, fare clic sul colore per visualizzare la tavolozza.</para>
                      <!-- <para>Defaults: red.</para> -->
                      </entry>
                  </row>
               </tbody>
          </tgroup>
          </informaltable>
	  </listitem>
	</varlistentry>
      </variablelist>
    </sect2>

    <sect2 id="gperfmeter-prefs-preferences"> 
    <title>Diagramma</title>
	<variablelist>
	  <varlistentry> 
	    <term> 
	      <guilabel>Grafico</guilabel> </term> 
	    <listitem>
	      <para>Per specificare il tipo di istogramma da utilizzare per la visualizzazione delle prestazioni, selezionare una delle seguenti opzioni:</para>
	      <itemizedlist>
		<listitem>
		  <para> 
		    <guilabel>Linea</guilabel> </para>
		  <para>Selezionare questa opzione per visualizzare un istogramma lineare.</para>
		</listitem>
		<listitem>
		  <para> 
		    <guilabel>Pieno</guilabel> </para>
		  <para>Selezionare questa opzione per visualizzare un istogramma pieno.</para>
		</listitem>
	      </itemizedlist>
	      <!-- <para>Default: 
		<guilabel>Line</guilabel>.</para>  -->
	    </listitem>
	  </varlistentry>
	  <varlistentry> 
	    <term> 
	      <guilabel>Disposizione</guilabel> </term> 
	    <listitem>
	      <para>Per specificare la modalit&agrave; di visualizzazione di pi&ugrave; diagrammi di prestazioni nella finestra della <application>&app;</application>, selezionare una delle seguenti opzioni:</para>
	      <itemizedlist>
		<listitem>
		  <para> 
		    <guilabel>Orizzontale</guilabel> </para>
		  <para>Selezionare questa opzione per visualizzare i diagrammi dei diversi parametri affiancati in senso orizzontale.</para>
		</listitem>
		<listitem>
		  <para> 
		    <guilabel>Verticale</guilabel> </para>
		  <para>Selezionare questa opzione per visualizzare i diagrammi dei diversi parametri in senso verticale.</para>
		</listitem>
	      </itemizedlist>
	      <!-- <para>Default: 
		<guilabel>Vertical</guilabel>.</para>  -->
	    </listitem>
	  </varlistentry>
	  <varlistentry> 
	    <term> 
	      <guilabel>Schermo</guilabel> </term> 
	    <listitem>
	      <para>Per specificare il livello di dettaglio con cui devono essere visualizzati i parametri relativi alle prestazioni, selezionare una delle seguenti opzioni:</para>
	      <itemizedlist>
		<listitem>
		  <para> 
		    <guilabel>Diagramma a barre</guilabel> </para>
		  <para>Selezionare questa opzione per visualizzare solo un diagramma a barre delle prestazioni correnti.</para>
		</listitem>
		<listitem>
		  <para> 
		    <guilabel>Istogramma</guilabel> </para>
		  <para>Selezionare questa opzione per visualizzare solo un istogramma con la cronologia delle prestazioni recenti.</para>
		</listitem>
		<listitem>
		  <para> 
		    <guilabel>Entrambi</guilabel> </para>
		  <para>Selezionare questa opzione per visualizzare sia l'istogramma che il diagramma a barre.</para>
		</listitem>
	      </itemizedlist>
	      <!-- <para>Default: 
		<guilabel>Both</guilabel>.</para>  -->
	    </listitem>
	  </varlistentry>
	  <varlistentry> 
	    <term> 
	      <guilabel>Linea limite</guilabel> </term> 
	    <listitem>
	      <para>Selezionare questa opzione per visualizzare una linea che rappresenti il valore di soglia nei diagrammi. </para>
	      <!-- <para>Default: unselected.</para>  -->
	    </listitem>
	  </varlistentry>
	  <varlistentry> 
	    <term> 
	      <guilabel>Nascondi barra dei menu</guilabel> </term> 
	    <listitem>
	      <para>Selezionare questa opzione per nascondere la barra dei menu. Fare clic nell'area di visualizzazione per tornare a visualizzare la barra dei menu.</para>
	      <!-- <para>Default: unselected.</para>  -->
	    </listitem>
	  </varlistentry>
	  <varlistentry> 
	    <term> 
	      <guilabel>Registra i campioni in</guilabel> </term> 
	    <listitem>
	      <para>Selezionare questa opzione per salvare in un file di log tutti i valori visualizzati dalla  <application>&app;</application> negli istogrammi. Usare la casella di testo per inserire il nome del file in cui dovranno essere salvati i dati.</para>
	      <!-- <para>Default: unselected.</para> -->
	    </listitem>
	  </varlistentry>
	  <varlistentry> 
	    <term> 
	      <guilabel>Rileva un campione ogni</guilabel> </term> 
	    <listitem>
	      <para>Usare questa casella di selezione per specificare l'intervallo con cui dovranno essere rilevati i valori delle prestazioni.</para>
	      <!-- <para>Default: 1 second.</para>  -->
	    </listitem>
	  </varlistentry>  
	</variablelist>
    </sect2>
  </sect1>
</article>
