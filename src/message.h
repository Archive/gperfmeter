
/*
 * $Header$
 *
 * Copyright (c) 1990-2004 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

#ifndef __MESSAGE_H__
#define __MESSAGE_H__

/* Message sets */

/* Message codes */

/* Warning codes */
#define PmWRN_HOSTNOTFOUND  1
#define PmWRN_HELPSELECT    2
#define PmWRN_HOSTCONNECT   3

/* Error codes */
#define PmERR_OPENDISPLAY  1
#define PmERR_OPENRESFILE  2
#define PmERR_SAVERESFILE  3
#define PmERR_LABELFONT    4
#define PmERR_HELPSELECT   5

#endif /* __MESSAGE_H__ */
