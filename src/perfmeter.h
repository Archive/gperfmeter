
/*
 * $Header$
 *
 * Copyright (c) 1990-2004 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

#ifndef __PERFMETER_H__
#define __PERFMETER_H__

#if defined(sun) && defined(__SVR4)
#define SOLARIS 1
#endif

#include <signal.h>
#include <sys/param.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <string.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <netdb.h>
#include <sys/wait.h>
#include <rpc/rpc.h>
#include <rpcsvc/rstat.h>
#ifdef SOLARIS
#include <sys/vmmeter.h>
#include <rpc/clnt_soc.h>
#endif /*SOLARIS*/
#include <locale.h>
#include <limits.h>
#include <netdb.h>
#include <nl_types.h>

#include <glib.h>
#include <glib/gi18n.h>
#include <gdk/gdk.h>
#include <gdk/gdkx.h>
#include <gtk/gtk.h>
#include <atk/atk.h>
#include <gconf/gconf-client.h>
#include "gnome-perfmeter.h"

#include <X11/Intrinsic.h>

#include "message.h"

/* To make lint happy. */
#define BIND_TEXTDOMAIN_CODESET  (void) bind_textdomain_codeset
#define BINDTEXTDOMAIN           (void) bindtextdomain
#define CLOSE                    (void) close
#define FCLOSE                   (void) fclose
#define FFLUSH                   (void) fflush
#define FPRINTF                  (void) fprintf
#define FPUTS                    (void) fputs
#define GETTIMEOFDAY             (void) gettimeofday
#define KILL                     (void) kill
#define MEMCPY                   (void) memcpy
#define MEMSET                   (void) memset
#define SETLOCALE                (void) setlocale
#define SETPGID                  (void) setpgid
#define SIGNAL                   (void) signal
#define SLEEP                    (void) sleep
#define STRCPY                   (void) strcpy
#define TEXTDOMAIN               (void) textdomain

#define	PMCOLORCHK               (void) PmColorChk
#define PMHIDEPROPS              (void) PmHideProps
#define PMLOGDATA                (void) PmLogData
#define PMMONITOR                (void) PmMonitor
#define PMRESETRESOURCES         (void) PmResetResources
#define PMSETDISPLAYLIST         (void) PmSetDisplayList
#define STRTOINT                 (void) StrToInt

/* Really should be defined in platforms' /usr/include/rpcsvc/rstat.h */
#ifndef RSTATVERS_VAR
#define RSTATVERS_VAR 4
#endif /*RSTATVERS_VAR*/

/* Limits */

#define PmMAX_CHARS        240         /* Max number of chars in a meter */
#define PmMAX_INT          INT_MAX
#define PmMAX_INTERVAL     3600        /* Max interval for hour and second */
#define PmMAX_VALUE        PmMAX_INT / FSCALE
#define PmMAX_LINE         256         /* Max length of character strings */
#define PmMAX_METERS       10          /* Max number of graphs/dials */
#define PmMAX_SAMPLETIME   3600        /* Max sample time in seconds (1hr) */
#define PmPAGE_LENGTH      60          /* Default sample file page length */
#define PmRPC_VERSION_NONE -1
#define PmTRIES            8
#define PmDATA_INCREMENT   100
#define PmMESSAGE_FILE     ""

#define EQUAL(a, b)    (strlen(a)==strlen(b)) & !strcmp(a, b)

/* Macros */

#define PmMax(a, b)         (((a) > (b)) ? (a) : (b))
#define PmMin(a, b)         (((a) <= (b)) ? (a) : (b))
#define PmAssignMax(a, b)   if ((b) > (a)) a = (b)
#define PmAssignMin(a, b)   if ((b) < (a)) a = (b)
#define PmAssign(a, b)      ((a) != (b) ? (a) = (b), TRUE : FALSE)

#define PmAssignGT(a, b, c) if ((b) > (c)) a = (b)
#define PmAssignLT(a, b, c) if ((b) < (c)) a = (b)
#define PmStrcpy(a, b)      if ((b)) STRCPY((a), (b))
#define PmRangeGT(a, b, c)  if ((a) > (b)) a = (c);
#define PmRangeLT(a, b, c)  if ((a) < (b)) a = (c);

typedef gchar *PmString;

typedef unsigned int CARD32;
typedef CARD32	BITS32;

/* Types */

typedef enum _ResType { 
    RES_STRING, RES_INT, RES_BOOL
} ResType;

typedef enum _PmMeterType { 
    PM_CPU, PM_LOAD, PM_DISK, PM_PAGE, PM_CNTX, PM_SWAP, PM_INTR, PM_PKTS,
    PM_COLL, PM_ERRS
} PmMeterType;

typedef enum _PmOrientationType { 
    PM_HORIZONTAL, PM_VERTICAL
} PmOrientationType;

typedef enum _PmGraphType { 
    PM_LINE, PM_SOLID
} PmGraphType;

/* Meter user interface */
typedef struct _PmUIMeterStruct {
    GdkGC *DrawGC;
    GdkGC *LimitGC;
} PmUIMeterStruct;

/* Perfmeter user interface */

typedef struct _PmUIStruct {
    int Argc;
    char **Argv;
    Display *Dsp;
    Screen *RScreen;
    Window RootWin;
    GdkColormap *ColorMap;
    gboolean BackingStore;
    gint mbHeight;                         /* Menubar height (in pixels). */
    guint TimerId;

    GtkUIManager *ui;
    GtkActionGroup *actions;

    GtkWidget *MainWin;
    GtkWidget *MenuBar;
    GtkWidget *VBox;

    GtkWidget *MeterHBox;   /* Box for meters (when displayed horizontally). */
    GtkWidget *MeterVBox;   /* Box for meters (when displayed vertically). */
    GtkWidget *GMeters[PmMAX_METERS]; /* gnome-perfmeter meter widgets. */
    GtkWidget *HostName;    /* Label for remote hostname (or "localhost"). */

    GtkAccelGroup *accel;   /* You need this for alt-hotkeys on menus */
    int initializing;       /* To work around some idiocy in gtk callbacks */
    GtkWidget *Aboutdialog;
    GtkWidget *PopupMenu;
    PmUIMeterStruct *Meters;
    GdkColor FgPixel;
    GdkColor BgPixel;
    PmString XmStrNULL;
    GdkBitmap *IconBM;
    GdkPixbuf *DeadPM;
    GdkPixbuf *SickPM;
    gboolean UseColor;               /* Later ... */
    GdkGC *CanvasGC;
    GdkFont *LabelFont;    
    GdkGC *LabelGC;
    XFontStruct *Font;
    GdkGC *FillGC;
    Region ClipRegion;
    Region EmptyRegion;
    GdkCursor *WaitCursor;
    int Busy;
    gboolean UICreated;
} PmUIStruct;

/* Meter data structure */
typedef struct _PmMeterStruct {
    PmMeterType Type;    /* Meter type */
    char *Name;          /* Meter name */
    char *RName;         /* Meter resource name */
    PmString TRName;     /* I18n meter resource name */
    PmString Label;      /* Meter label */
    int MaxMax;          /* Maximum value of max */
    int MinMax;          /* Minimum value of max */
    int CurMax;          /* Current value of max */
    int Limit;           /* Maximum limit */
    int RangeInc;        /* Increment */
    int Scale;           /* Scale factor */
    int Showing;         /* Set if this meter to be displayed */
    int LongAvg;         /* Long (hour hand) average */
    int UnderCnt;        /* Count of times under max */
    double Value;        /* Actual value (for saving samples and ceiling). */
    int *Data;           /* Actual data values */
    GdkColor MeterColor;
    GdkColor LimitColor;
    gboolean Redisplay;  /* Redisplay Flag */
} PmMeterStruct;

/* Perfmeter structure */
typedef struct _PmStruct {
    int Argc;                      /* Command line argument count */
    char **Argv;                   /* Command line argument list  */
    char *AppName;                 /* Application name */
    char *AppClass;                /* Application class */
    int ParentPID;                 /* Parent process ID */
    int ChildPID;                  /* Child process ID */
    gboolean Active;
    gboolean Showing;
    gboolean IsRemote;             /* Remote host */
    gboolean IsDead;               /* Is remote machine dead? */
    gboolean IsSick;               /* Is remote machine sick? */
    gboolean Debug;                /* Debug flag */
    gboolean Resizeable;           /* Resize window - change in # of graphs? */
    int LockCount;                 /* Lock count */

    /* Geometry related */
    int Rows;                      /* Number of rows */
    int Cols;                      /* Number of columns */
    int MeterWp;                   /* Meter width */
    int MeterHp;                   /* Meter height */
    int AreaWp;                    /* Area width */
    int AreaHp;                    /* Area height */

    unsigned int DialWp;           /* Dial icon width */
    unsigned int DialHp;           /* Dial icon height */
    unsigned int DeadWp;           /* Dead icon width */
    unsigned int DeadHp;           /* Dead icon height */
    unsigned int SickWp;           /* Sick icon width */
    unsigned int SickHp;           /* Sick icon height */
    int OldSocket;                 /* Old socket connection id */
    int RpcVersion;                /* RPC version number */
    gchar *Hostname;               /* Name of host being metered */
    char *LocalHost;               /* Local host name */
    char *LocalAddr;               /* Local host address */
    char *LocalMachine;            /* Name of local machine */
    char *Machine;
    int SampleInterval;            /* Sample seconds */
    int ShortExp;
    int LongExp;
    FILE *SampleFP;                /* File descriptor for sample file */
    gchar *LogFilename;
    gboolean Log;
    gboolean ShowLimit;            /* Show ceiling limit line */
    int LogLineCount;
    int LogPageLength;
    gboolean CollectWhenIconized;
    gboolean ShowHostName;
    gboolean ShowMenubar;
    gboolean ShowStripChart;
    gboolean ShowLiveActionBar;
    int SampleModifier;
    gboolean inFrontPanel;
    gboolean isTooltipDisplayed;
    int LabStart;
    gboolean StripChartActivity;
    gboolean ShowTitle;
    int LabelHp;                   /* Meter label max height */
    int LabelWp;                   /* Meter Label max width */
    int ValueWp;                   /* Value label max width */
    PmMeterStruct *Meters;         /* [PmMAX_METERS] */
    PmOrientationType Orientation; /* Direction for dials/graphs. */
    int WrapCount;                 /* Number of meters in minor dimension */
    PmGraphType GraphType;         /* Graph type (line or solid graph) */
    int DisplayList[PmMAX_METERS]; /* Display list of visible meters */
    int NMeters;                   /* Number of Visible meters */
    int MData;                     /* Max number of data points */
    int NData;                     /* Number of data points */
    int CData;                     /* Current data point */
    PmString *MachineNames;
    int NumMachineNames;
    GdkColor CanvasColor;
    GdkColor LabelColor;
    gboolean AutoLoadByHost;
    gboolean SaveByHost;
    gboolean LoadByHost;
    char SaveFilename[PATH_MAX];
    GConfClient *gconf_client;
} PmStruct;

/* Host data structure */
typedef struct _PmDataStruct {
    int IsInit;
    CLIENT *Client;
    CLIENT *OldClient;
    statstime StatsTime;
#ifdef SOLARIS
    statsvar StatsVar;
#endif /*SOLARIS*/
    int CpuStates;
    int NDrive;
    int TotalPackets;           
    int TotalErrors;
    int TotalCollisons;
    struct timeval Timeval;
    int *Xfer1;
    int BadCnt;
    int *OldTime;
    struct timeval OldTimeval;
    int OldPage;
    int OldSwap;
    int OldIntr;
    int OldSwtch;
    int Data[PmMAX_METERS];
} PmDataStruct;

extern PmStruct *PM;
extern PmDataStruct *PMD;
extern PmUIStruct *UI;

/* Function prototypes */
extern void add_menu_items(void);
extern void PmLoadResources(int, char **);
extern void PmShow(gboolean);
extern void PmShowProps(void);
extern gboolean PmHideProps(void);
extern void PmWarning(int, char *);
extern void PmError(int, char *);
extern void PmSetRedisplayInterrupt(void);
extern void PmResizeMeterData(int);
extern void PmResetMeter(int, gboolean);
extern void PmLock(void);
extern void PmUnLock(void);
extern gboolean PmSetDisplayList(int *);
extern void PmRedisplay(void);
extern void PmScaleData(void);
extern int  PmLogData(void);
extern void PmSetGraphType(PmGraphType);
extern void PmSetMinSize(void);
extern void PmSignalHandler(int);
extern void PmSetSignalHandlers(void);
extern void PmResetMeterData(void);
extern gboolean PmMonitor(void);
extern void PmSetMeterColor(int);
extern void PmSetColors(void);
extern void PmSaveResources();
extern gchar *expand_tilde(const char *);
extern void PmResetProps(void);
extern void PmRefresh(gboolean);
extern void PmResetCurMax(int);
extern gboolean PmResetResources(char *);
extern void PmShowSaveDialog();
extern void PmSetScaleFactors(void);
extern void PmReset(char*);
extern void PmResetPropsDecor(void);
extern void PmSaveCommand();
extern int PmUpdateData(void);
extern void PmLogClose(void);
extern int PmLogOpen(void);
extern int PmConnectToHost(void);
extern void PmAddHostToList(char *);
extern void PmBusy(gboolean);
extern void PmPropsBusy(gboolean);
extern void PmPropsSetDisplayList(void);
extern void PmAllocateColor(int, gboolean);
extern void PmSaveCommand(void);

extern void set_hostname(void);
extern void ToggleDirection(gint);
extern void ToggleMenuBar(int);
extern void ValidateStripBarMenu(void);
extern gint InterruptTimer(gpointer);

#define PmMAX_HOST_LIST_SIZE	10  /* Max. # of remote hosts in list. */
#define PmMAX_IP_ADDR_LEN       24  /* IpV4 (16 bytes), IpV6(24 bytes).*/
#define PmMAX_IPADDR_BUF_SIZE   124 /* ( 5 * 24 + (5-1)( for separator)) */

GList *remote_hosts;

#endif /* __PERFMETER_H__ */
