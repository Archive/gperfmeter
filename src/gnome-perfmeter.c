
/*
 * $Header$
 *
 * Copyright (c) 1990-2004 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	    See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/* gnome-perfmeter meter widget. */

#include <stdio.h>
#include "gnome-perfmeter.h"
#include <gtk/gtkobject.h>
#include <gtk/gtkhbox.h>
#include <gtk/gtkvbox.h>
#include <gtk/gtkdrawingarea.h>
#include <gtk/gtkeventbox.h>
#include <gtk/gtklabel.h>
#include <gtk/gtkhseparator.h>
#include <gtk/gtkvseparator.h>

static void gnome_perfmeter_class_init     (GnomePerfmeterClass *class);
static void gnome_perfmeter_instance_init  (GnomePerfmeter      *gc);
static void gnome_perfmeter_destroy        (GtkObject           *object);
static void gnome_perfmeter_finalize       (GObject             *object);

static GtkVBoxClass *parent_class = NULL;


GtkType
gnome_perfmeter_get_type(void)
{
    static GType perf_type = 0;

    if (!perf_type) {
        static const GTypeInfo perf_info = {
            sizeof(GnomePerfmeterClass),
            NULL,                      /* base_init */
            NULL,                      /* base_finalize */
            (GClassInitFunc) gnome_perfmeter_class_init,
            NULL,                      /* class_finalize */
            NULL,                      /* class_data */
            sizeof(GnomePerfmeter),
            0,                         /* n_preallocs */
            (GInstanceInitFunc) gnome_perfmeter_instance_init,
        };

        perf_type = g_type_register_static(GTK_TYPE_HBOX, "GnomePerfmeter",
                                           &perf_info, 0);
    }

    return(perf_type);
}


static void
gnome_perfmeter_class_init(GnomePerfmeterClass *class)
{
    GtkObjectClass *object_class;
    GObjectClass *gobject_class;

    object_class = (GtkObjectClass *) class;
    gobject_class = (GObjectClass *) class;
    object_class->destroy = gnome_perfmeter_destroy;
    gobject_class->finalize = gnome_perfmeter_finalize;
    parent_class = g_type_class_peek_parent(class);
}


static void
gnome_perfmeter_instance_init(GnomePerfmeter *gp)
{
    gp->vbox = gtk_vbox_new(FALSE, 0);

    gp->meter_type_event_box = gtk_event_box_new();
    gtk_box_pack_start(GTK_BOX(gp->vbox), gp->meter_type_event_box, 
                       FALSE, FALSE, 0);
    gtk_widget_show(gp->meter_type_event_box);

    gp->meter_type = gtk_label_new(NULL);
    gtk_container_add(GTK_CONTAINER(gp->meter_type_event_box), gp->meter_type);
    gtk_widget_show(gp->meter_type);

    gp->chart = gtk_drawing_area_new();
    gtk_widget_set_events(gp->chart, GDK_BUTTON_PRESS_MASK);
    gtk_widget_show(gp->chart);
    gtk_box_pack_start(GTK_BOX(gp->vbox), gp->chart, TRUE, TRUE, 2);

    gp->maxval_event_box = gtk_event_box_new();
    gtk_box_pack_start(GTK_BOX(gp->vbox), gp->maxval_event_box, 
                       FALSE, FALSE, 0);
    gtk_widget_show(gp->maxval_event_box);

    gp->maxval = gtk_label_new(NULL);
    gtk_container_add(GTK_CONTAINER(gp->maxval_event_box), gp->maxval);
    gtk_widget_show(gp->maxval);

    gp->hsep = gtk_hseparator_new();
    gtk_box_pack_start(GTK_BOX(gp->vbox), gp->hsep, FALSE, FALSE, 0);

    gp->vsep = gtk_vseparator_new();
    gtk_box_pack_start(GTK_BOX(gp), gp->vbox, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(gp), gp->vsep, FALSE, FALSE, 0);
}


GtkWidget *
gnome_perfmeter_new(void)
{
    GnomePerfmeter *gperfmeter;

    gperfmeter = g_object_new(gnome_perfmeter_get_type(), NULL);

    return(GTK_WIDGET(gperfmeter));
}


void
gnome_perfmeter_set_maxval(GnomePerfmeter *gp, const gchar *str)
{
    gtk_label_set_text(GTK_LABEL(gp->maxval), str);
}


void
gnome_perfmeter_set_meter_type(GnomePerfmeter *gp, const gchar *str)
{
    gtk_label_set_text(GTK_LABEL(gp->meter_type), str);
}


void
gnome_perfmeter_set_chart_bg_color(GnomePerfmeter *gp, GdkColor color)
{
    GtkStyle *style;

    style = gtk_widget_get_style(gp->chart);
    style->bg[GTK_STATE_NORMAL] = color;
    gtk_widget_set_style(gp->chart, style);
}


void
gnome_perfmeter_clear_chart(GnomePerfmeter *gp, gboolean redisplay)
{
    if (gp->chart->window != NULL) {
        if (redisplay == TRUE) {
            gdk_window_clear_area_e(gp->chart->window, 0, 0, 0, 0);
        } else {
            gdk_window_clear_area(gp->chart->window, 0, 0, 0, 0);
        }
    }
}


GtkWidget *
gnome_perfmeter_get_chart(GnomePerfmeter *gp)
{
    return(gp->chart);
}


GdkWindow *
gnome_perfmeter_get_chart_win(GnomePerfmeter *gp)
{
    return(gp->chart->window);
}


gint
gnome_perfmeter_get_chart_width(GnomePerfmeter *gp)
{
    return(gp->chart->allocation.width);
}


gint
gnome_perfmeter_get_chart_height(GnomePerfmeter *gp)
{
    return(gp->chart->allocation.height);
}


gint
gnome_perfmeter_get_min_width(GnomePerfmeter *gp)
{
    int valw = gp->maxval->requisition.width;
    int typew = gp->meter_type->requisition.width;

    return(((valw > typew) ? valw : typew) + 2);
}


gint
gnome_perfmeter_get_min_height(GnomePerfmeter *gp)
{
    return(gp->maxval->requisition.height + 
           gp->meter_type->requisition.height + 20);
}


void
gnome_perfmeter_show_hsep(GnomePerfmeter *gp, gboolean state)
{
    if (state == TRUE) {
        gtk_widget_show(gp->hsep);
    } else {
        gtk_widget_hide(gp->hsep);
    }
}


void
gnome_perfmeter_show_vsep(GnomePerfmeter *gp, gboolean state)
{
    if (state == TRUE) {
        gtk_widget_show(gp->vsep);
    } else {
        gtk_widget_hide(gp->vsep);
    }
}


static void
gnome_perfmeter_destroy(GtkObject *object)
{
    /* Remember, destroy can be run multiple times! */

    g_return_if_fail(object != NULL);
    g_return_if_fail(GNOME_IS_PERFMETER(object));

    GTK_OBJECT_CLASS(parent_class)->destroy(object);
}


static void
gnome_perfmeter_finalize(GObject *object)
{
    g_return_if_fail(object != NULL);
    g_return_if_fail(GNOME_IS_PERFMETER(object));

    G_OBJECT_CLASS(parent_class)->finalize(object);
}
