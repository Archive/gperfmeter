
/*
 * $Header$
 *
 * Copyright (c) 1990-2004 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.     See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/* gnome-perfmeter meter widget. */

#ifndef __GNOME_PERFMETER_H__
#define __GNOME_PERFMETER_H__

#include <gdk/gdk.h>
#include <gtk/gtkhbox.h>
#include <gtk/gtkvbox.h>
#include <gtk/gtkwidget.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


#define GNOME_TYPE_PERFMETER            (gnome_perfmeter_get_type())
#define GNOME_PERFMETER(obj)            (GTK_CHECK_CAST((obj), GNOME_TYPE_PERFMETER, GnomePerfmeter))
#define GNOME_PERFMETER_CLASS(klass)    (GTK_CHECK_CLASS_CAST((klass), GNOME_TYPE_PERFMETER, GnomePerfClass))
#define GNOME_IS_PERFMETER(obj)         (GTK_CHECK_TYPE((obj), GNOME_TYPE_PERFMETER))
#define GNOME_IS_PERFMETER_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GNOME_TYPE_PERFMETER))
#define GNOME_PERFMETER_GET_CLASS(obj)  (GTK_CHECK_GET_CLASS((obj), GNOME_TYPE_PERFMETER, GnomePerfmeterClass))


typedef struct _GnomePerfmeter       GnomePerfmeter;
typedef struct _GnomePerfmeterClass  GnomePerfmeterClass;

struct _GnomePerfmeter {
    GtkHBox hbox;

    /* private */
    GtkWidget *maxval;
    GtkWidget *maxval_event_box;
    GtkWidget *chart;
    GtkWidget *meter_type;
    GtkWidget *meter_type_event_box;
    GtkWidget *vbox;
    GtkWidget *hsep;
    GtkWidget *vsep;
};

struct _GnomePerfmeterClass {
    GtkVBoxClass parent_class;
};


GtkType     gnome_perfmeter_get_type           (void) G_GNUC_CONST;
GtkWidget * gnome_perfmeter_new                (void);
 
void        gnome_perfmeter_set_maxval         (GnomePerfmeter *gp,
                                                const gchar *str);

void        gnome_perfmeter_set_meter_type     (GnomePerfmeter *gp,
                                                const gchar *str);

void        gnome_perfmeter_set_chart_bg_color (GnomePerfmeter *gp,
                                                GdkColor color);

void        gnome_perfmeter_clear_chart        (GnomePerfmeter *gp,
                                                gboolean redisplay);

GtkWidget * gnome_perfmeter_get_chart          (GnomePerfmeter *gp);
GdkWindow * gnome_perfmeter_get_chart_win      (GnomePerfmeter *gp);

gint        gnome_perfmeter_get_chart_width    (GnomePerfmeter *gp);
gint        gnome_perfmeter_get_chart_height   (GnomePerfmeter *gp);

gint        gnome_perfmeter_get_min_width      (GnomePerfmeter *gp);
gint        gnome_perfmeter_get_min_height     (GnomePerfmeter *gp);

void        gnome_perfmeter_show_hsep          (GnomePerfmeter *gp,
                                                gboolean state);
void        gnome_perfmeter_show_vsep          (GnomePerfmeter *gp,
                                                gboolean state);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __GNOME_PERFMETER_H__ */
