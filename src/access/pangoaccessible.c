
/*
 * $Header$
 *
 * Copyright 2002-2004 Sun Microsystems Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#include <stdio.h>
#include <string.h>
#include <gtk/gtk.h>
#include <gtk/gtkwidget.h>
#include <sys/types.h>
#include <pango/pango-layout.h>
#include <atk/atkobject.h>

#include "pangoaccessible.h"
#include "../perfmeter.h"

static gint pango_accessible_get_n_children(AtkObject *);

static void pango_accessible_class_init(PangoAccessibleClass *);
static void pango_accessible_finalize(GObject *);
static void atk_value_interface_init(AtkValueIface *);

/* AtkValue Interfaces */

static void pango_accessible_get_current_value(AtkValue *, GValue *);
static void pango_accessible_get_maximum_value(AtkValue *, GValue *);
static void pango_accessible_get_minimum_value(AtkValue *, GValue *);
static gboolean pango_accessible_set_current_value(AtkValue *, const GValue *);

static gpointer parent_class = NULL;
extern AtkObject *pango_accessible[];


GType
pango_accessible_get_type(void)
{
    static GType type = 0;

    if (!type) {
        static const GTypeInfo tinfo = {
            sizeof(PangoAccessibleClass),                 /* class size */
            (GBaseInitFunc) NULL,                         /* base init */
            (GBaseFinalizeFunc) NULL,                     /* base finalize */
            (GClassInitFunc) pango_accessible_class_init, /* class init */
            (GClassFinalizeFunc) NULL,                    /* class finalize */
            NULL,                                         /* class data */
            sizeof(PangoAccessible),                      /* instance size */
            0,                                            /* nb preallocs */
            (GInstanceInitFunc) NULL,                     /* instance init */
            NULL                                          /* value table */
        };

        static const GInterfaceInfo atk_value_info = {
            (GInterfaceInitFunc) atk_value_interface_init,
            (GInterfaceFinalizeFunc) NULL,
            NULL
        };

        type = g_type_register_static(GTK_TYPE_ACCESSIBLE,
                                      "PangoAccessible", &tinfo, 0);
        g_type_add_interface_static(type, ATK_TYPE_VALUE, &atk_value_info);
    }

    return(type);
}


static void
pango_accessible_class_init(PangoAccessibleClass *klass)
{
    GObjectClass *gobject_class = G_OBJECT_CLASS(klass);
    AtkObjectClass *class = ATK_OBJECT_CLASS(klass);

    g_return_if_fail(class != NULL);
    parent_class = g_type_class_peek_parent(klass);

    gobject_class->finalize = pango_accessible_finalize;

    class->get_n_children = pango_accessible_get_n_children;
}


AtkObject *
pango_accessible_new(PangoLayout *obj)
{
    GObject *object;
    AtkObject *accessible;
    PangoAccessible *cp;

    object = g_object_new(PANGO_TYPE_ACCESSIBLE, NULL);
    g_return_val_if_fail(object != NULL, NULL);

    accessible = ATK_OBJECT(object);
    atk_object_initialize(accessible, obj);

    cp = PANGO_ACCESSIBLE(object);
    cp->playout = obj;

    accessible->role = ATK_ROLE_DRAWING_AREA;

    return(accessible);
}


/* Report the number of children as 0. */

/*ARGSUSED*/
static gint
pango_accessible_get_n_children(AtkObject *obj)
{
    return(0);
}


static void
pango_accessible_finalize(GObject *object)
{
    G_OBJECT_CLASS(parent_class)->finalize(object);
}


static gint 
get_accessible_meter_index(AtkObject *obj) 
{
    gint i;

    for (i = 0; i < PmMAX_METERS; i++) {
        if (pango_accessible[i] == obj) {
            return(i);
        }
    }

    return(-1);
}


static void        
pango_accessible_get_current_value(AtkValue *obj, GValue *value)
{
    gdouble current_value;

    int i = get_accessible_meter_index(ATK_OBJECT(obj));
    
    if (i < 0) {
        current_value = -1;
    } else {
        current_value = PM->Meters[i].Value;
    }

    (void) memset(value,  0, sizeof (GValue));
    g_value_init(value, G_TYPE_DOUBLE);
    g_value_set_double(value, current_value);
}


static void        
pango_accessible_get_maximum_value(AtkValue *obj, GValue *value)
{
    gint maximum_value;

    int i = get_accessible_meter_index(ATK_OBJECT(obj));
    
    if (i < 0) {
        maximum_value = -1;
    } else {
        maximum_value = PM->Meters[i].CurMax;
    }

    (void) memset(value,  0, sizeof(GValue));
    g_value_init(value, G_TYPE_INT);
    g_value_set_int(value, maximum_value);
}


/*ARGSUSED*/
static void        
pango_accessible_get_minimum_value(AtkValue *obj, GValue *value)
{
    g_value_init(value, G_TYPE_INT);
    g_value_set_int(value, 0);
}


/*ARGSUSED*/
static gboolean    
pango_accessible_set_current_value(AtkValue *obj, const GValue *value)
{
    return(FALSE);
}


static void 
atk_value_interface_init(AtkValueIface *iface) 
{
    g_return_if_fail(iface != NULL);

    iface->get_current_value = pango_accessible_get_current_value;
    iface->get_maximum_value = pango_accessible_get_maximum_value;
    iface->get_minimum_value = pango_accessible_get_minimum_value;
    iface->set_current_value = pango_accessible_set_current_value;
}
